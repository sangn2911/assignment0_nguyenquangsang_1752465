package Assignment9;

public class Warrior {
	private	int baseHP;
	private	int wp;

	public Warrior(int baseHP, int wp) {
		if (baseHP < 1 || baseHP > 888) {System.out.println("Error ... BaseHP must be 1-888");System.exit(0);}
		if (wp < 0 || wp > 1) {System.out.println("Error ... Weapon must be type 0-1");System.exit(0);} 
		this.baseHP = baseHP;
		this.wp = wp;
	}
	public int getBaseHP() {
		return this.baseHP;
	}
	public int getwp() {
		return this.wp;
	}
	double getRealHP() {
		if (wp == 1)  return baseHP;
		else return (float) baseHP/10.0;
	}
}
