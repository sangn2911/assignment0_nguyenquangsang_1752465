package Assignment9;

public class Knight {
	private	int baseHP;
	private	int wp;
	
	public Knight(int baseHP, int wp) {
		if (baseHP < 99 || baseHP > 999) {System.out.println("Error ... BaseHP must be 99-999");System.exit(0);}
		if (wp < 0 || wp > 1) {System.out.println("Error ... Weapon must be type 0-1");System.exit(0);} 
		this.baseHP = baseHP;
		this.wp = wp;
	}
	public int getBaseHP() {
		return this.baseHP;
	}
	public int getwp() {
		return this.wp;
	}
	double getRealHP() {
		if (wp == 1)  return baseHP;
		else return (float) baseHP/10.0;
	}	
}
